Feature: Scans application for packages and classes
    As a CS teacher
    I want to test students' programming assignments
    In order to find out if classes have been correctly created

    Background:
        Given the maximum grade is 100
        Given the main class is 'Pratica72'
        Given I set the script timeout to 3000
        Given I evaluate 'import utfpr.ct.dainf.grader.*;'
        Given I evaluate 'import utfpr.ct.dainf.if62c.pratica.*'
        Given I evaluate 'String testFilePath = ClassLoader.getSystemClassLoader().getResource("testdata.txt").getFile();' 
        And I evaluate 'java.io.File testFile = new java.io.File(testFilePath);'
        Given I evaluate 'String shortTestFilePath = ClassLoader.getSystemClassLoader().getResource("shorttestdata.txt").getFile();' 
        And I evaluate 'java.io.File shortTestFile = new java.io.File(shortTestFilePath);'
        Given I evaluate 'String inputFilePath = ClassLoader.getSystemClassLoader().getResource("input1.txt").getFile();' 
        And I evaluate 'java.io.File inputFile = new java.io.File(inputFilePath);'
    
    Scenario: Verifica se a classe Pratica7.2 existe e está corretamente declarada
        Given I report 'Iniciando avaliação...'
        Given I report 'Avaliando item 3...'
        Given class 'Pratica72' exists store class in <mainClass>
        Given class <mainClass> declares 'main(java.lang.String[])' method save in <mainMethod>
        And method <mainMethod> returns type 'void'
        And member <mainMethod> has 'public' modifier
        And member <mainMethod> has 'static' modifier
        Then award 10 points

    Scenario: Verifica se a classe ContadorPalavras existe e está corretamente implementada
        Given I report 'Avaliando item 4...'
        Given class 'utfpr.ct.dainf.if62c.pratica.ContadorPalavras' exists store class in <cpClass>
        Then award 5 points
        And class <cpClass> declares 'ContadorPalavras(java.lang.String)' constructor save in <cpConst>
        Then award 10 points

    Scenario: Verifica se a classe ContadorPalavras declara e inicializa a propriedade reader
        Given I report 'Avaliando item 5...'
        Given class <cpClass> declares field 'reader' save in <readerField>
        And field <readerField> is of type 'java.io.BufferedReader'
        Then award 5 points
        Given member <readerField> has 'private' modifier
        Then award 10 points

    Scenario: Verifica se a classe ContaPalavras implementa o método getPalavras()
        Given I report 'Avaliando item 6...'
        Given class <cpClass> declares 'getPalavras()' method save in <gpMethod>
        Then award 10 points
        Given I evaluate 'cpTest = new ContadorPalavras(testFilePath)'
        And I evaluate 'gpTest = cpTest.getPalavras()'
        And expression 'gpTest instanceof java.util.Map' evaluates to <true>
        Then award 10 points

    Scenario: Verifica se o método getPalavras() retorna a contagem de palavras correta
        Given I report 'Avaliando item 7...'
        Given I evaluate 'cpTest = new ContadorPalavras(testFilePath)'
        And I evaluate 'gpTest = cpTest.getPalavras()'
        And expression 'gpTest.get("Cheque")' evaluates to <3>
        And expression 'gpTest.get("compensado")' evaluates to <2>
        Then award 20 points

    Scenario: Verifica o arquivo de saída é gerado corretamente
        Given I report 'Avaliando item 8...'
        Given I evaluate 'inputData = testFilePath + "\n"'
        Given I set input from <inputData>
        Given I evaluate 'Pratica72.main(new String[0])'
        And I evaluate 'java.io.File outFile = new java.io.File(testFilePath + ".out");'
        And I evaluate 'bufReader = new BufferedReader(new FileReader(outFile))'
        And I evaluate 'firstLine = bufReader.readLine()'
        And <firstLine> matches regex '^\s*Cheque\s*,\s*3.*$'
        Then award 20 points

    Scenario: Report final grade.
        Given I set output to <default>
        Given I report grade formatted as 'FINAL GRADE: %.1f'
